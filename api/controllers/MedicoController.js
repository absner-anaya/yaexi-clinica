/**
 * MedicoController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

    create: function(req,res) {
        Medico.create(req.body).exec((error, data) => {
            if (error) {
                return res.status(500).json({
                    code: 500,
                    message: 'Error almacenando datos',
                    content: error
                });
            } else {
                return res.status(200).json({
                    code: 200,
                    message: 'Data almacenada exitosamente',
                    content: data
                });
            }
        })
    },
    update: function(req,res) {
        Medico.update({id: req.params.id}, req.body).exec((error, medico)  =>  {
            if (error) {
                return  res.status(500).send({
                    code:500,
                    message: 'Error al actualizar datos',
                    content: error
                })
            } else {
                return res.status(200).send({
                    code: 200,
                    message: 'Datos actualizados correctamente',
                    content: medico
                })
            }
        })
    },
    find: function(req,res) {
        Medico.find().exec((error, medico)    =>  {
            if (error) {
                return res.status(500).send({
                    code: 500,
                    message: 'Error al obtener datos',
                    content: error
                })
            } else {
                return res.status(200).send({
                    code: 200,
                    message: 'Datos obtenidos exitosamente',
                    content: medico
                })
            }
        })
    },
    findOne: function(req,res) {
        Medico.findOne({id: req.params.id}).exec((error, medico)  =>  {
            if (error) {
                return res.status(500).send({
                    code: 500,
                    message: 'Error al obtener datos',
                    content: error
                })
            } else {
                return res.status(200).send({
                    code: 200,
                    message: 'Datos obtenidos exitosamente',
                    content: medico
                })
            }
        })
    },
    destroy: function (req, res) {
        Medico.destroy({id: req.params.id}).exec(function (err, cita) {
            if(err){
                return res.status(500).send({
                    code: 500,
                    message: 'Error al eliminar datos',
                    content: cita
                });
            }else{
                return res.status(200).send({
                    code: 200,
                    message: 'user delete',
                    content: cita
                });
            }
        });
    }

};

