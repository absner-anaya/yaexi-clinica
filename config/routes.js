/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {


  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/

  '/': 'HomeController.index',

  /** Rutas cliente */
  'POST /client':                              'ClienteController.create',
  'PUT  /client/:id':                          'ClienteController.update',
  'GET  /client':                              'ClienteController.find',
  'GET  /client/:id':                          'ClienteController.findOne',
  'DEL  /client/:id':                          'ClienteController.destroy',

  /** Rutas citas */
  'POST /cita':                                'CitaController.create',
  'PUT  /cita/:id':                            'CitaController.update',
  'GET  /cita':                                'CitaController.find',
  'GET  /cita/:id':                            'CitaController.findOne',
  'DEL  /cita/:id':                            'CitaController.destroy',

  /** Rutas examen */
  'POST /examen':                              'ExamenController.create',
  'PUT  /examen/:id':                          'ExamenController.update',
  'GET  /examen':                              'ExamenController.find',
  'GET  /examen/:id':                          'ExamenController.findOne',
  'DEL  /examen/:id':                          'ExamenController.destroy',

  /** Rutas Medico */
  'POST /medico':                              'MedicoController.create',
  'PUT  /medico/:id':                          'MedicoController.update',
  'GET  /medico':                              'MedicoController.find',
  'GET  /medico/:id':                          'MedicoController.findOne',
  'DEL  /medico/:id':                          'MedicoController.destroy',

  /** Rutas Análisis */
  'POST /analisis':                            'AnalisisController.create',
  'PUT  /analisis/:id':                        'AnalisisController.update',
  'GET  /analisis':                            'AnalisisController.find',
  'GET  /analisis/:id':                        'AnalisisController.findOne',
  'DEL  /analisis/:id':                        'AnalisisController.destroy',

  /** Rutas Historia */
  'POST /historia':                            'HistoriaController.create',
  'PUT  /historia/:id':                        'HistoriaController.update',
  'GET  /historia':                            'HistoriaController.find',
  'GET  /historia/:id':                        'HistoriaController.findOne',
  'DEL  /historia/:id':                        'HistoriaController.destroy',
  
  //  ╔═╗╔═╗╦  ╔═╗╔╗╔╔╦╗╔═╗╔═╗╦╔╗╔╔╦╗╔═╗
  //  ╠═╣╠═╝║  ║╣ ║║║ ║║╠═╝║ ║║║║║ ║ ╚═╗
  //  ╩ ╩╩  ╩  ╚═╝╝╚╝═╩╝╩  ╚═╝╩╝╚╝ ╩ ╚═╝



  //  ╦ ╦╔═╗╔╗ ╦ ╦╔═╗╔═╗╦╔═╔═╗
  //  ║║║║╣ ╠╩╗╠═╣║ ║║ ║╠╩╗╚═╗
  //  ╚╩╝╚═╝╚═╝╩ ╩╚═╝╚═╝╩ ╩╚═╝


  //  ╔╦╗╦╔═╗╔═╗
  //  ║║║║╚═╗║
  //  ╩ ╩╩╚═╝╚═╝


};
